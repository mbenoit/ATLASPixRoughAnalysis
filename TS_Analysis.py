from ROOT import *
import itertools
from collections import defaultdict
from array import array

peary_basefolder="/home/peary/peary/PEARYDATA"


def read_peary_raw_data():
    filename = peary_basefolder + "/test/data.txt"
    f = open(filename, "r")
    lines = f.readlines()
    lines = [line for line in lines if "X:" not in line]
    lines = [line for line in lines if "BUSY" not in line]
    lines = [line for line in lines if "BUFFER" not in line]
    lines = [line for line in lines if "SERDES" not in line]
    lines = [line for line in lines if "WEIRD" not in line]
    lines = [line for line in lines if "TRIGGER" not in line]

    unsorted_data = []

    for line in lines:
        words = line.split()
        hit = [int(word) for word in words[1:]]
        unsorted_data.append(hit)
        # print hit

    events = []

    sorted_event = {}

    def eventnr(x):
        ev = 0
        if (len(x)) > 4:
            ev = x[6]
        else:
            ev = x[0]
        # print ev
        return ev

    for key, group in itertools.groupby(unsorted_data, key=lambda x: eventnr(x)):
        sorted_event[key] = list(group)
        # print sorted_event[key]

    sorted_event = defaultdict(lambda: [], sorted_event)

    return sorted_event


def printEvents(data):

    for event in data:
        print data[event]
        a = raw_input("next")


def plotTS1vsEvent(data):

    gr = TGraph()

    for event in data:
        fpga_ts=data[event][0][5]
        delta = ((data[event][0][5]/2) & 0x3FF)
        delta = delta - (data[event][0][2] &0x3FF)
        gr.SetPoint(event,event,delta)

    gr.Draw("")
    a=raw_input()




if __name__ == '__main__':
    data = read_peary_raw_data()
    plotTS1vsEvent(data)