from ROOT import *
import itertools
from collections import defaultdict
from array import array
from math import *
import numpy as np

peary_basefolder="/srv/data/data0/201808-cern_h8-unigetel/peary_data"
tel_basefolder="/srv/data/data0/201808-cern_h8-unigetel/tel_data"

def read_peary_raw_data(runnumber):

    filename= peary_basefolder + "/test/data.txt"
    f=open(filename,"r")
    lines = f.readlines()
    lines = [line for line in lines if "X:" not in line]
    lines = [line for line in lines if "BUSY" not in line]
    lines = [line for line in lines if "BUFFER" not in line]
    lines = [line for line in lines if "SERDES" not in line]
    lines = [line for line in lines if "WEIRD" not in line]
    #lines = [line for line in lines if "TRIGGER" not in line]

    unsorted_data=[]

    for line in lines:
        words=line.split()
        hit = [int(word) for word in words[1:]]
        unsorted_data.append(hit)
        #print hit

    events = []

    sorted_event={}
    
    def eventnr(x):
        ev=0
        if(len(x))>4:
            ev= x[6]
        else : 
            ev= x[0]
        #print ev
        return ev
    
    
    for key,group in itertools.groupby(unsorted_data,key=lambda x:eventnr(x)):
        sorted_event[key]=list(group)
        #print sorted_event[key]


    sorted_event=defaultdict(lambda : [],sorted_event)

    return sorted_event


def build_peary_tree(events, f):
    print "building peary tree"

    maxevent = max(events, key=int)
    # maxevent=10000

    f.cd()
    peary_tree=TTree("Hits","peary data tree")

    maxn=1000000
    col = array('i',maxn*[0])
    row = array('i',maxn*[0])
    ts1 = array('i',maxn*[0])
    ts2 = array('i',maxn*[0])
    tot = array('L',maxn*[0])
    BinCnt = array('i',maxn*[0])
    trigger_cnt = array('i',[0])
    fpga_ts = array('L',[0])
    nhits = array('i',[0])
    Timing = array('i',maxn*[0])

    peary_tree.Branch('NHits',nhits,"nhits/I")
    peary_tree.Branch('PixX',col,"col[nhits]/I")
    peary_tree.Branch('PixY',row,"row[nhits]/I")
    peary_tree.Branch('ts1',ts1,"ts1[nhits]/I")
    peary_tree.Branch('ts2',ts2,"ts2[nhits]/I")
    peary_tree.Branch("tot",tot,"tot[nhits]/L")
    peary_tree.Branch('trigger_cnt',trigger_cnt,"trigger_cnt/I")
    peary_tree.Branch('fpga_ts',fpga_ts,"fpga_ts/L")
    peary_tree.Branch('BinCnt',BinCnt,"BinCnt[nhits]/I")
    peary_tree.Branch('Timing',Timing,"Timing[nhits]/I")

    for ev in range(maxevent+1):
        data = events[ev]
        for i, hit in enumerate(data):
            if (len(hit) == 2):
                fpga_ts[0] = hit[1]
                trigger_cnt[0] = hit[0]
                col[i] = -1
                row[i] = -1
            if (len(hit) >= 7):
                col[i] = hit[0]
                row[i] = hit[1]
                ts1[i] = hit[2]
                ts2[i] = hit[3]
                tot[i]=hit[4]
                BinCnt[i]=hit[6]
                Timing[i]=hit[8]

                #print ts1[i],ts2[i],totpix

        nhits[0] = len(data)
        peary_tree.Fill()
    f.mkdir("Plane6");
    f.cd("Plane6");
    peary_tree.Write()
    return peary_tree


def get_telescope_tree(run,rf):

    filename=tel_basefolder + "/cosmic_%06i/cosmic_%06i.root"%(run,run)

    f= TFile(filename,"open")

    rf.cd()

    telescope_ts_tree = f.Event.CloneTree()
    telescope_data_tree_0 = f.Get('Plane0/Hits').CloneTree()
    telescope_data_tree_1 = f.Get('Plane1/Hits').CloneTree()
    telescope_data_tree_2 = f.Get('Plane2/Hits').CloneTree()
    telescope_data_tree_3 = f.Get('Plane3/Hits').CloneTree()
    telescope_data_tree_4 = f.Get('Plane4/Hits').CloneTree()
    telescope_data_tree_5 = f.Get('Plane5/Hits').CloneTree()

    telescope_data_tree_0.AddFriend(telescope_ts_tree)
    telescope_data_tree_1.AddFriend(telescope_ts_tree)
    telescope_data_tree_2.AddFriend(telescope_ts_tree)
    telescope_data_tree_3.AddFriend(telescope_ts_tree)
    telescope_data_tree_4.AddFriend(telescope_ts_tree)
    telescope_data_tree_5.AddFriend(telescope_ts_tree)

    rf.cd()
    telescope_ts_tree.Write()
    
    rf.mkdir("Plane0"); rf.cd("Plane0"); telescope_data_tree_0.Write()
    rf.mkdir("Plane1"); rf.cd("Plane1"); telescope_data_tree_1.Write()    
    rf.mkdir("Plane2"); rf.cd("Plane2"); telescope_data_tree_2.Write()    
    rf.mkdir("Plane3"); rf.cd("Plane3"); telescope_data_tree_3.Write()    
    rf.mkdir("Plane4"); rf.cd("Plane4"); telescope_data_tree_4.Write()    
    rf.mkdir("Plane5"); rf.cd("Plane5"); telescope_data_tree_5.Write()     
    
    f.Close()

    #return telescope_ts_tree,telescope_data_tree




def plot_spatial_correlation(filename,plotfolder,runnumber):

    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    tel_hit_tree= f.Get("Plane0/Hits")
    tel_hit_tree.AddFriend(peary_tree)

    corxx = TH2I("corxx","corxx",25,0,25,80,0,80)
    corxy = TH2I("corxy","corxy",25,0,25,336,0,336)
    coryx = TH2I("coryx","coryx",400,0,400,80,0,80)
    coryy = TH2I("coryy","coryy",400,0,400,336,0,336)

    peary_tree.Print()
    tel_hit_tree.Print()

    telev=tel_hit_tree.GetEntries()
    dutev=peary_tree.GetEntries()

    misf = open("run_summary_ATLASPixA09_1e14.dat","a")

    if(telev==dutev):
        print "Number of event matching !! %i == %i"%(telev,dutev)
        misf.write("Run %i SYNCED "%(runnumber))
    else:
        print "MISMATCH!! %i != %i"%(telev,dutev)
        misf.write("Run %i DESYNCED "%(runnumber))
    misf.close()
    print "Computing spatial correlation"
    for ev,event in enumerate(tel_hit_tree):
        if(ev%100000==0):
            print "Event #%i"%ev
        for i in range(peary_tree.NHits):
            for j in range(tel_hit_tree.NHits):
                #print (peary_tree.PixX[i],tel_hit_tree.PixX[j])
                corxx.Fill(peary_tree.PixX[i],tel_hit_tree.PixX[j])
                corxy.Fill(peary_tree.PixX[i],tel_hit_tree.PixY[j])
                coryx.Fill(peary_tree.PixY[i],tel_hit_tree.PixX[j])
                coryy.Fill(peary_tree.PixY[i],tel_hit_tree.PixY[j])

    can = TCanvas()
    corxx.Draw("colz")
    can2 = TCanvas()
    coryy.Draw("colz")

    can.SetLogz()
    can2.SetLogz()

    can.SaveAs(plotfolder+"/CorXX_DUTxPlane0_run%i.png"%runnumber)
    can2.SaveAs(plotfolder+"/CorYY_DUTxPlane0_run%i.png"%runnumber)
    can.SaveAs(plotfolder+"/CorXX_DUTxPlane0_run%i.root"%runnumber)
    can2.SaveAs(plotfolder+"/CorYY_DUTxPlane0_run%i.root"%runnumber)
    a=raw_input()



def plot_spatial_correlation_vs_time(filename,plotfolder,runnumber):

    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    tel_hit_tree= f.Get("Plane0/Hits")
    tel_hit_tree.AddFriend(peary_tree)

    telev=tel_hit_tree.GetEntries()
    dutev=peary_tree.GetEntries()


    corxxt = TH2I("corxxt","corxxt",10000,0,dutev,2*160,-160,160)
    coryyt = TH2I("coryyt","coryyt",10000,0,dutev,2*400,-400,400)

    peary_tree.Print()
    tel_hit_tree.Print()



    if(telev==dutev):
        print "Number of event matching !! %i == %i"%(telev,dutev)
    else:
        print "MISMATCH!! %i != %i"%(telev,dutev)

    print "Computing correlation vs time"

    for ev,event in enumerate(tel_hit_tree):
        if(ev%100000==0):
            print "Event #%i"%ev
        for i in range(peary_tree.NHits):
            for j in range(tel_hit_tree.NHits):
                corxxt.Fill(ev,peary_tree.PixX[i]-tel_hit_tree.PixX[j])
                coryyt.Fill(ev,(400-peary_tree.PixY[i])-tel_hit_tree.PixY[j])

    can3 = TCanvas()
    corxxt.Draw("colz")
    can4 = TCanvas()
    coryyt.Draw("colz")

    can3.SetLogz()
    can4.SetLogz()

    can3.SaveAs(plotfolder+"/CorXXt_DUTxPlane0_run%i.png"%runnumber)
    can4.SaveAs(plotfolder+"/CorYYt_DUTxPlane0_run%i.png"%runnumber)
    can3.SaveAs(plotfolder+"/CorXXt_DUTxPlane0_run%i.root"%runnumber)
    can4.SaveAs(plotfolder+"/CorYYt_DUTxPlane0_run%i.root"%runnumber)


#
def plotTiming(filename,plotfolder,runnumber):
    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    can = TCanvas()
    peary_tree.Draw("Timing >> h125(100,-50,50)","tot>5","norm")
    can2=TCanvas()
    peary_tree.Draw("Timing >> h25(50,-50,50)","tot>5","norm")


    can.SaveAs(plotfolder+"/Timing_80MHz_run%06i.png"%runnumber)
    can.SaveAs(plotfolder+"/Timing_80MHz_run%06i.root"%runnumber)
    can2.SaveAs(plotfolder+"/Timing_40MHz_run%06i.png"%runnumber)
    can2.SaveAs(plotfolder+"/Timing_40MHz_run%06i.root"%runnumber)
    a=raw_input()



def plotTimeWalk(filename,plotfolder,runnumber):
    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    peary_tree.Draw("Timing:tot >> h(64,0,64,20,-40,0)","tot>5","colz")
    h=gDirectory.Get("h")

    prof = h.ProfileX()

    prof.Draw("same")
    a=raw_input()


def plotTOT(filename,plotfolder,runnumber):

    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    can = TCanvas()
    peary_tree.Draw("tot >> tot(63,1,64)","Timing>-50 && Timing <50","norm")
    tot.GetYaxis().SetRangeUser(0,0.1)
    can.SaveAs(plotfolder+"/TOT_run%06i.png"%runnumber)
    can.SaveAs(plotfolder+"/TOT_run%06i.root"%runnumber)
    a=raw_input()



def ComputeNoise(filename,plotfolder,runnumber,armduration=5000,clock=6.25e-9):
    f=TFile(filename,"rw")

    noise = 0

    peary_tree = f.Get("Plane6/Hits")
    for event in peary_tree:
        if event.NHits > 0:
           for i in range(event.NHits):
               if(event.PixX[i]>0 and event.PixY[i]>0):
                   noise+=1

    noise = noise/((armduration*clock*peary_tree.GetEntries())*25*400) # total hits / integration time * number of pixels
    print "noise is %3.5f Hz/pixel"%noise
    return noise



def plotHitmap(filename,plotfolder,runnumber) :

    f = TFile(filename, "rw")

    peary_tree = f.Get("Plane6/Hits")
    #tel_ts_tree = f.Event
    #tel_ts_tree.AddFriend(peary_tree)
    histo = TH2I("hitmap", "hitmap",25, 0,25,400,0, 400)

    print "Computing hitmap for DUT"
    for event in peary_tree:
        if event.NHits > 0:
            for i in range(event.NHits):
                histo.Fill(event.PixX[i],event.PixY[i])

    HP = []
    for i in range(histo.GetNbinsX()+1) :
        for j in range(histo.GetNbinsY()+1):
            if(histo.GetBinContent(i,j)>=10):
                #print "%i,%i %i"%(i-1,j-1,histo.GetBinContent(i,j))
                HP.append([i-1,j-1])
    print "# of hot pixels : %i"%(len(HP))

    for pix in HP :
        print "MaskPixel %i %i 0"%(pix[0],pix[1])
    can5= TCanvas()
    hitmap.Draw("colz")
    can5.SaveAs(plotfolder+"/DUTHitmap_run%i.png"%runnumber)
    can5.SaveAs(plotfolder+"/DUTHitmap_run%i.root"%runnumber)

    a=raw_input()




def plotMeanTimingVsRow(filename,plotfolder,runnumber):


    f=TFile(filename,"r")
    #peary_tree = f.Get("Plane6/Hits")
    peary_tree = f.Get("dut0_atlaspix1/tracks_clusters_matched")


    means = []

    for row in range(400):
        peary_tree.Draw("clu_time >> histo(40,-40,0)","clu_row==%i"%row,"")
        h=gDirectory.Get("histo")
        # res=h.Fit("gaus","S")
        # if(res.IsValid()):
        #     print res.Parameter(1)
        #     means.append(res.Parameter(1))
        # else:
        #     means.append(0)
        means.append(h.GetMean())
        print "processing row %i"%row

    gr = TGraph(400)

    for row in range(400):
        gr.SetPoint(row,row,means[row])

    res=gr.Fit("pol1","S","",50,400)
    drift = res.FittedFunction()

    histocorr=TH1D("corr_timing","Corrected Timing",225,-112.5e-9,112.5e-9)
    hitstovrow = TH2I("no","no",10,-40,0,400,0,400)
    hitstovrowcorr = TH2D("cor","cor",225,-112.5e-9,112.5e-9,400,0,400)

    mean = 2227 * 6.25e-9
    for i,event in enumerate(peary_tree):
        if i%10000==0:
            print "event %i"%i
        # for hit in range(event.NHits):
        #     if(event.tot[hit]>5):
        #newentry = event.clu_time*6.25e-9 - floor(drift.GetFunction().Eval(event.clu_row))*6.25e-9 -12.5e-9
        if(isnan(event.clu_row)==False):
            newentry = event.clu_time*6.25e-9 - means[int(event.clu_row)]*6.25e-9 -12.5e-9

        #print newentry
            histocorr.Fill(newentry)
            hitstovrowcorr.Fill(newentry,event.clu_row)
            hitstovrow.Fill(event.clu_time,event.clu_row)
        #print event.Timing[hit],event.PixY[hit]

    can = TCanvas()
    gr.Draw("AL")
    can2 = TCanvas()
    histocorr.DrawNormalized()
    can3 = TCanvas()
    hitstovrow.Draw("colz")
    can4 = TCanvas()
    hitstovrowcorr.Draw("colz")


    a = raw_input()




def plot_ts_correlation(filename):


    f=TFile(filename,"rw")

    peary_tree = f.Get("Plane6/Hits")
    tel_ts_tree= f.Get("Event")
    tel_ts_tree.AddFriend(peary_tree)

    print filename
    peary_tree.Print()
    tel_ts_tree.Print()

    corrt = TGraph()



    dut_period=1.0/80e6
    tel_period=1.0/40e6

    tel_to=1656687314*tel_period
    dut_to=1825119286*dut_period

    f1 = TF1("f1","[0] + [1]*x")
    f1.SetParameter(0,-7.3)
    f1.SetParameter(1,1)
    #histo.Fit("f1")

    i=0

    diffs = []

    for i,event in enumerate(tel_ts_tree):
        corrt.SetPoint(i, tel_ts_tree.TriggerTime * tel_period , peary_tree.fpga_ts * dut_period )
        diff=(tel_ts_tree.TriggerTime * tel_period) -( peary_tree.fpga_ts * dut_period)
        diffs.append(diff)



    meandiff= np.mean(diffs)
    rmsdiff= np.std(diffs)

    diffvt= TGraph()
    diffhist = TH1D("","",10000,meandiff-3*rmsdiff,meandiff+3*rmsdiff)


    for i,diff in enumerate(diffs):
        diffvt.SetPoint(i,i,diff)
        diffhist.Fill(diff)



    can = TCanvas()
    corrt.SetMarkerSize(5)
    corrt.Draw("ap")

    can2 = TCanvas()
    diffvt.Draw("ap")

    can3 = TCanvas()
    diffhist.Draw()


    a=raw_input()




if __name__ == '__main__':


    runs =  range(10451,10476)

    params = []

    hvs = [65,45,35,90,95]
    ths=[0.9,0.89,0.88,0.875,0.87]

    for hv in hvs:
        for th in ths:
            params.append([hv,th])


    fnoise=open("Noise_ap1b01JN04.dat","a")

    #f.write("HV     Threshold   Noise (Hz/pixel) \n")



    for i,run in enumerate(runs):

        print "Run %i"%run
        filename = "RoughAnalysis_%06i.root"%run
        f=TFile("RoughAnalysis_%06i.root"%run,"recreate")
        #
        peary_events=read_peary_raw_data(run)
        peary_tree=build_peary_tree(peary_events,f)
        #telescope_ts_tree, telescope_data_tree=get_telescope_tree(run,f)
        get_telescope_tree(run,f)
        #
        f.Close()

        noise = ComputeNoise(filename,"",run)
        fnoise.write("%f %f %f \n"%(params[i][0],params[i][1],noise))





