from ROOT import *
import itertools
from collections import defaultdict
from array import array

data_folder="/srv/data/data0/201806-cern_h8-unigetel/reco_local"


def findDesync(runnumber):



    synccutfile = open("run_summary_ATLASPixA09_1e14.dat","a")

    f=TFile(data_folder + "/run%06i-match-trees.root"%runnumber)
    matched_tree = f.Get("dut0_atlaspix1/tracks_clusters_matched")
    unmatched_tree = f.Get("dut0_atlaspix1/clusters_unmatched")

    print runnumber
    matched_tree.Print()
    dutev=matched_tree.GetEntries()

    bins=int(dutev/500)


    corxxt = TH2I("corxxt","corxxt",bins,0,dutev,2*160,-160,160)
    coryyt = TH2I("coryyt","coryyt",bins,0,dutev,2*400,-400,400)
    coryytunmatched = TH2I("coryyt","coryyt",bins,0,dutev,2*400,-400,400)

    for ev,event in enumerate(matched_tree):
        if(ev%100000==0):
            print "Event #%i"%ev

        corxxt.Fill(ev,event.trk_u-event.clu_u)
        coryyt.Fill(ev,event.trk_v-event.clu_v)

    offset = 2

    for ev,event in enumerate(matched_tree):

        unmatched_tree.GetEntry(ev+offset)

        if(ev%100000==0):
            print "Event #%i"%ev

        coryytunmatched.Fill(ev, event.trk_v - unmatched_tree.clu_v)

    profy= coryyt.ProfileX()


    can2 = TCanvas()
    coryytunmatched.Draw("colz")
    can3 = TCanvas()
    corxxt.Draw("colz")
    can4 = TCanvas()
    coryyt.Draw("colz")
    cut=-1
    for i in range(profy.GetNbinsX()+1):
        mean=profy.GetBinContent(i)
        error=profy.GetBinError(i)
        #print "mean : %f error : %f"%(mean,error)
        if(error>15):
            print "DESYNC at event :  %i +- 500"%(i*500)
            print "Cutting at event %i"%(i*500-1000)
            cut = (i*500-1000)
            can4.cd()
            line = TLine(cut,-400,cut,400)
            line.SetLineWidth(2)
            line.SetLineColor(ROOT.kRed)
            line.Draw()
            can4.Update()
            break

    synccutfile.write(" CUT %i \n"%(cut))




    can = TCanvas()
    profy.Draw()












if __name__ == '__main__':


    for run in range(3077,3476):
        plotCorrelationVsTime(run)
