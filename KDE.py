from ROOT import *
import itertools
from collections import defaultdict,OrderedDict
from array import array

import numpy as np
import matplotlib.pyplot as plt

from scipy import interpolate
from scipy.interpolate import UnivariateSpline
from scipy.stats.kde import gaussian_kde

from progress.bar import Bar

#gROOT.SetBatch()
#ROOT.EnableImplicitMT()




def GenerateDataset(n = 1000) :



    nbin = 100
    xmin = 0
    xmax = 10

    can = TCanvas()

    h1 = TH1D("h1", "h1", nbin, xmin, xmax)



    data = []

    for i in range(n):
        if(i < 0.4 * n):
            data.append(gRandom.Gaus(2,1))
        else:
            data.append(gRandom.Gaus(7,1.5))
        h1.Fill(data[i])


    h1.Scale(1. / h1.Integral(), "width" );
    h1.SetStats(False);
    h1.SetTitle("Bi-Gaussian");
    h1.Draw();

    f1 = TF1("f1", "0.4*ROOT::Math::normal_pdf(x,1,2)+0.6*ROOT::Math::normal_pdf(x,1.5,7)", xmin, xmax);
    f1.SetLineColor(kGreen + 2);
    f1.Draw("SAME");

    return [n,data,xmin,xmax,can,h1,f1]



def FitKDE(dataset):


    rho=1.0
    kdecore = TKDE(dataset[0],dataset[1],dataset[2],dataset[3],"",rho)
    kdecore.Draw("SAME")

    return kdecore


if __name__ == '__main__':


    dataset=GenerateDataset()
    kdecore=FitKDE(dataset)