import os


plotfolder = "/srv/data/data0/201806-cern_h8-unigetel/ATLASPixRoughAnalysisData"


runlist = range(2111,2200)


for run in runlist :
    filename = plotfolder + "/RoughAnalysis_%06i.root" % run
    #os.system("rm %s"%filename)
    os.system("python PlotCorrelation.py %i"%run)