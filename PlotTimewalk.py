from ROOT import *
import itertools
from collections import defaultdict
from array import array
import numpy as np


datafolder = "/home/peary/peary/RoughDataAnalysis/Timewalk_0.875"

def ReadRawFile(filename):


    data = open(filename,"r")
    lines = data.readlines()

    lines.pop(0)


    x = []
    y= []

    for line in lines:
        words=line.split(",")

        x.append(float(words[0]))
        y.append(int(words[1]))

    return [x,y]


def PlotHisto(data):

    nbins = len(data[0])
    xmin = data[0][0]
    xmax = data[0][nbins-1]


    histo = TH1D("","",nbins,xmin,xmax)

    for i,x in enumerate(data[0]):
        histo.Fill(x,data[1][i])

    histo.Draw()

    a=raw_input()

def PlotTimewalkHisto(dataset,pulses):

    nbinsy = 1000
    ymin = 5e-6
    ymax = 6e-6
    nbinsx=16
    xmin=0*(5900./3.6)/0.3
    xmax=0.8*(5900./3.6)/0.3

    print nbinsx,xmin,xmax,nbinsy,ymin,ymax

    nppulses = np.asarray(pulses)

    histo=TH2D("","",nbinsx,xmin,xmax,nbinsy,ymin,ymax)

    for i,data in enumerate(dataset):
        for j,x in enumerate(data[0]):
            histo.Fill(pulses[i]*(5900./3.6)/0.3,x,data[1][j])

    histo.Draw("colz")

    a = raw_input()







if __name__ == '__main__':

    filename = datafolder + "/" + "M1_0.25v_.87500000.txt"

    dataset = []

    pulses = [0.2,0.25,0.3000,0.4,0.5,0.8]

    for i,datafile in enumerate(["M1_0.2v_.87500000.txt","M1_0.25v_.87500000.txt",  "M1_0.3v_.87500000.txt",  "M1_0.4v_.87500000.txt",  "M1_0.5v_.87500000.txt", "M1_0.8v_.87500000.txt"]):
        filename = datafolder + "/" + datafile
        data=ReadRawFile(filename)
        dataset.append(data)
        #PlotHisto(data)

    PlotTimewalkHisto(dataset,pulses)
