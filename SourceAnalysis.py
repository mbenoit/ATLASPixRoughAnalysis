from TreeBuilder import build_peary_tree
from KDE import *
from ROOT import *
import itertools
from collections import defaultdict,OrderedDict
from array import array

import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import UnivariateSpline
from scipy.stats.kde import gaussian_kde

from progress.bar import *

gROOT.SetBatch()
#ROOT.EnableImplicitMT()


datadir= "/home/peary/peary/PEARYDATA"
plotdir= "/home/peary/peary/PEARYDATA"


def MakeSourceTree(rawdatafilename,rootfile):

    print "[SourceTree] parsing data file " + rawdatafilename


    nlines = len(open(rawdatafilename, "r").readlines())

    f = open(rawdatafilename, "r")

    rootfile.cd()

    peary_tree=TTree("Hits","peary data tree")

    maxn=10000
    col = array('i',maxn*[0])
    row = array('i',maxn*[0])
    ts1 = array('i',maxn*[0])
    ts2 = array('i',maxn*[0])
    tot = array('L',maxn*[0])
    BinCnt = array('i',maxn*[0])
    trigger_cnt = array('i',[0])
    fpga_ts = array('L',[0])
    nhits = array('i',[0])
    Timing = array('i',maxn*[0])

    peary_tree.Branch('NHits',nhits,"nhits/I")
    peary_tree.Branch('PixX',col,"col[nhits]/I")
    peary_tree.Branch('PixY',row,"row[nhits]/I")
    peary_tree.Branch('ts1',ts1,"ts1[nhits]/I")
    peary_tree.Branch('ts2',ts2,"ts2[nhits]/I")
    peary_tree.Branch("tot",tot,"tot[nhits]/L")
    peary_tree.Branch('trigger_cnt',trigger_cnt,"trigger_cnt/I")
    peary_tree.Branch('fpga_ts',fpga_ts,"fpga_ts/L")
    peary_tree.Branch('BinCnt',BinCnt,"BinCnt[nhits]/I")
    peary_tree.Branch('Timing',Timing,"Timing[nhits]/I")

    unsorted_data = []


    bar = ChargingBar('Processing', max=nlines,suffix= '%(percent).1f%% - %(eta)ds')



    for line in f :
        if "HIT" in line:
            words = line.split()
            hit = [int(word) for word in words[1:]]
            #print hit
            col[0] = hit[0]
            row[0] = hit[1]
            ts1[0] = hit[2]
            ts2[0] = hit[3]
            tot[0] = hit[4]
            BinCnt[0] = hit[6]
            Timing[0] = hit[8]
            nhits[0]=1
            peary_tree.Fill()
        bar.next()

    bar.finish()

    peary_tree.Write()
    rootfile.Write()


def plotTOTSource(events):

    tothisto= TH1I("tot","tot",64,0,64)
    XYMap = TH2I("hitmap","hitmap",25,0,25,400,0,400)

    maxevent= max(events,key=int)
    for ev in range(maxevent+1):
        data = events[ev]
        for i,hit in enumerate(data):
            if(len(hit)>=7):
                tothisto.Fill(hit[4])
                XYMap.Fill(hit[0],hit[1])

    return tothisto,XYMap


def plotTOT(rootfile):
    pearytree = rootfile.Get("Hits")
    can = TCanvas()
    pearytree.Draw("tot >> h(64,0,64)")
    h.Draw()
    can2= TCanvas()
    pearytree.Draw("PixY:PixX >> h2(25,0,25,400,0,400)","","colz")
    rootfile.cd()
    h.Write()
    h2.Write()
    rootfile.Write()

def plotTiming(pearytree):

    pearytree.Draw("Timing >> t(2048,0,2048)")


def plotTSDelta(pearytree):

    previous_ts = 0

    histo = TH1I("","",1000000,0,1000000)

    for i in range(1,pearytree.GetEntries()):

        pearytree.GetEvent(i-1)
        previous_ts=pearytree.fpga_ts


        pearytree.GetEvent(i)
        current_ts = pearytree.fpga_ts
        histo.Fill(current_ts-previous_ts)
        print previous_ts,current_ts,current_ts-previous_ts

    histo.Draw()


def plotCountVsThreshold(datasets,vminus=0.6,device=""):

    gr = TGraph()
    grspl= TGraph()
    dgr = TGraph()

    gr.SetName("gr")
    grspl.SetName("grspl")
    dgr.SetName("dgr")
    i=0

    x=[]
    y=[]

    settmp = OrderedDict(sorted(datasets.items()))

    for key, set in settmp.iteritems():
        rootfilename = "%s/%s_Fe55/%s_Fe55_VNFBPix20_VNPix40_ckdivend7_80MHz_th%0.03f_vm%0.02f.root"%(plotdir,device,device,key,vminus)
        rootfile = TFile(rootfilename, "open")

        tree = rootfile.Get("Hits")
        cnt = 0
        for event in tree:
            if (event.tot > 0):
                cnt += 1
        print key,tree.GetEntries(),cnt



        gr.SetPoint(i,key-0.8,cnt)
        x.append(key)
        y.append(cnt)

        i+=1
        rootfile.Close()

    can = TCanvas()
    gr.Draw("AC*")
    spl = UnivariateSpline(x, y, k=4, s=0)
    dspl = spl.derivative()


    for i,x in enumerate(np.arange(0.865,1.06,0.005)):
        grspl.SetPoint(i,x-0.8,spl(x))
        dgr.SetPoint(i,x-0.8,-dspl(x))
        print i,x,dspl(x)

    grspl.Draw("Lsame")

    can2=TCanvas()
    dgr.Draw("AC*")

    rootfile= TFile("%s/%s_Fe55/Threshold_scan_%s_vm%0.02f.root"%(plotdir,device,device,vminus),"update")
    rootfile.cd()
    grspl.Write()
    dgr.Write()
    gr.Write()
    rootfile.Close()


def plotSpectrumPerPixel(rootfile):

    tree=rootfile.Get("Hits")
    rootfile.mkdir("PixelSpectrum")
    rootfile.cd("PixelSpectrum")

    histo = {}
    #data = {}

    print "Creating histogram array"
    for col in range(25):
        for row in range(400):
            histo["X%i_Y%i"%(col,row)] = TH1I("X%i_Y%i"%(col,row),"X%i_Y%i"%(col,row),64,0,64)
            #data["X%i_Y%i"%(col,row)] = np.array([],dtype=int)

    print "Filling histograms"

    nevents = tree.GetEntries()

    bar = ChargingBar('Processing', max=nevents,suffix= '%(percent).1f%% - %(eta)ds')
    bar.start()


    for i,event in enumerate(tree):
        if(event.PixX[0]<25 and  event.PixY[0]< 400):
            histo["X%i_Y%i" % (event.PixX[0],event.PixY[0])].Fill(event.tot[0])
        #data["X%i_Y%i" % (event.PixX[0],event.PixY[0])]=np.append(data["X%i_Y%i" % (event.PixX[0],event.PixY[0])],event.tot[0])
        if i%10000==0:
            bar.goto(i)

        # if i==200000:
        #     break

    bar.finish()



    print "Writing to file"

    bar = ChargingBar('Processing', max=10000,suffix= '%(percent).1f%% - %(eta)ds')
    bar.start()


    rootfile.cd()

    Fe55Peaks = TH2I("Fe55_peaks","Fe55 peak position",25,0,25,400,0,400)
    Fe55PeaksDist = TH1I("Fe55_peaks_distribution","Fe55 peak position distribution",64,0,64)


    rootfile.cd("PixelSpectrum")

    for col in range(25):
        for row in range(400):
            can = TCanvas()
            histo["X%i_Y%i"%(col,row)].Draw()

            # Peak search with TSPectrum
            spectrum = TSpectrum()
            spectrum.Search(histo["X%i_Y%i"%(col,row)])
            spectrum.Print("")
            n = spectrum.GetNPeaks()

            # # KDE fit of spectrum
            #
            # n_sample=len(data["X%i_Y%i"%(col,row)])
            # dataset = [n_sample,data["X%i_Y%i"%(col,row)],0,64,can]
            # print dataset
            # kde=FitKDE(dataset)
            # # kdefunc= kde.GetDrawnFunction()
            # # kdefunc.Write()

            if n>0:
                print spectrum.GetPositionX()[n-1]
                peak = spectrum.GetPositionX()[n-1]
                Fe55Peaks.Fill(col,row,peak)
                Fe55PeaksDist.Fill(peak)

            can.Write()
            bar.next()

    rootfile.cd()
    Fe55Peaks.Write()
    Fe55PeaksDist.Write()

    bar.finish()

    rootfile.Write()



if __name__ == '__main__':



    dth=0.005
    thresholds = [(0.865 +i*dth) for i in range(30)]
    vminuses = [0.60]
    device = "ap1b02w06s14"


    # rootfile = TFile("%s/%s_Fe55_VNPix50/%s_Fe55_VNFBPix20_VNPix50_ckdivend7_80MHz.root"%(plotdir,device,device), "recreate")
    # MakeSourceTree("%s/%s_Fe55_VNPix50/data.txt"%(datadir,device),rootfile)
    # rootfile = TFile("%s/%s_Fe55_VNPix50/%s_Fe55_VNFBPix20_VNPix50_ckdivend7_80MHz.root"%(plotdir,device,device), "update")
    # plotTOT(rootfile)
    # plotSpectrumPerPixel(rootfile)



    for vm in vminuses:
        datasets = {}
        for th in thresholds:
            print "th %0.3fV vm %0.2fV"%(th,vm)
            datasets[th]="%s/%s_Fe55_VNpix50/data_%0.03fV_VMinus%0.02fV.txt"%(datadir,device,th,vm)
            rootfile = TFile("%s/%s_Fe55_VNpix50/%s_Fe55_VNFBPix20_VNPix50_ckdivend7_80MHz_th%0.03f_vm%0.02f.root"%(plotdir,device,device,th,vm), "recreate")
            MakeSourceTree("%s/%s_Fe55_VNpix50/data_%0.03fV_VMinus%0.02fV.txt"%(datadir,device,th,vm),rootfile)
        plotCountVsThreshold(datasets,vminus=vm,device=device)


#    bigdataset = ["Fe55_0.875_ckdiv7_45V"]

    # datasets ={
    #            #0.855:"SourceScan_60s_Fe55_0.855_ckdiv7_45V",
    #            0.865:"SourceScan_60s_Fe55_0.865_ckdiv7_45V",
    #            0.875:"SourceScan_60s_Fe55_0.875_ckdiv7_45V",
    #            0.89:"SourceScan_60s_Fe55_0.89_ckdiv7_45V",
    #            0.9:"SourceScan_60s_Fe55_0.9_ckdiv7_45V",
    #            0.92:"SourceScan_60s_Fe55_0.92_ckdiv7_45V",
    #            0.94:"SourceScan_60s_Fe55_0.94_ckdiv7_45V",
    #            0.96:"SourceScan_60s_Fe55_0.96_ckdiv7_45V",
    #            0.98:"SourceScan_60s_Fe55_0.98_ckdiv7_45V",
    #            1:"SourceScan_60s_Fe55_1.0_ckdiv7_45V",
    #            1.02:"SourceScan_60s_Fe55_1.02_ckdiv7_45V",
    #            1.05:"SourceScan_60s_Fe55_1.05_ckdiv7_45V",
    #            1.1:"SourceScan_60s_Fe55_1.1_ckdiv7_45V",
    #            1.2:"SourceScan_60s_Fe55_1.2_ckdiv7_45V",
    #            1.3:"SourceScan_60s_Fe55_1.3_ckdiv7_45V"
    #            }

    #plotCountVsThreshold(datasets)


    # for set in bigdataset:
    #     print set
    #     rawdatafile = datadir+ "/" + set + "/data.txt"
    #     rootfilename = plotdir + "/RoughAnalysis_%s.root"%set
    #     print rootfilename
    #     rootfile=TFile(rootfilename,"recreate")
    #     MakeSourceTree(rawdatafile,rootfile)
    #     plotSpectrumPerPixel(rootfile)
    #     rootfile.Write()
    #     rootfile.Close()










    # filename = datadir + "/Fe55_0.875_ckdiv7_45V/data.txt"
    # rootfilename = plotdir + "/RoughAnalysis_Fe55_0.875_ckdiv7_45V.root"
    #
    # rootfile=TFile(rootfilename,"recreate")
    # MakeSourceTree(filename,rootfile)
    #
    #
    # # tot,XYMap=plotTOTSource(peary_events)
    # #
    # # can = TCanvas()
    # # tot.Draw()
    # # can2=TCanvas()
    # # XYMap.Draw("colz")
    # #
    # # tot.Write()
    # # XYMap.Write()
    #
    # rootfile.Write()
    #
    # #peary_tree=build_peary_tree(peary_events,f)
    # #plotTSDelta(peary_tree)
    # #plotTOT(peary_tree)
    # #plotTiming(peary_tree)
    #
    # #a=raw_input()
    #
    # rootfile.Close()
