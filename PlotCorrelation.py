from TreeBuilder import *
from SyncTagger import *
import argparse
from ROOT import *
#ROOT.EnableImplicitMT()
#gROOT.SetBatch()
import os.path



plotfolder = "/srv/data/data0/201806-cern_h8-unigetel/ATLASPixRoughAnalysisData"
plotfolder= "/home/peary/peary/RoughDataAnalysis/SourceStudy_ap1b02w23s15"



if __name__ == '__main__':

    parse = argparse.ArgumentParser("Plotter for ATLASPix vs Telescope correlation")
    parse.add_argument("runnumber",help="Run number to plot",type=int)
    args = parse.parse_args()

    print "run number is %i"%args.runnumber

    if(os.path.isfile(filename)!=True):
        print "TTree file not exisiting in this folder, building it"
        f=TFile(filename,"recreate")
        peary_events=read_peary_raw_data(333)
        peary_tree=build_peary_tree(peary_events,f)
        #get_telescope_tree(run,f)
        f.Close()

    #plot correlation and hitmap

    #if(os.path.isfile(data_folder+"/run%06i-match-trees.root"%run)==True):
    #plot_spatial_correlation(filename,plotfolder,run)
        #findDesync(run)

    #plot_spatial_correlation_vs_time(filename,plotfolder,run)
    plotHitmap(filename,plotfolder,333)
    #plot_ts_correlation(filename)


